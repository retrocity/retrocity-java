package site.retrocity.common

import kotlinx.serialization.Serializable
import site.retrocity.common.model.RetroState

interface ConnectedSession<OUT> {

    val user: String

    suspend fun send(data: OUT)

    suspend fun close()
}

interface ConnectedSessionListener<IN> {

    fun onMessage(data: IN)

    fun onSessionClosed()
}

@Serializable
data class RetrospectiveUpdate(
    val state: RetroState,
    val timer: Long,
    val participants: List<Participant>
) {

    @Serializable
    data class Participant(
            val user: String,
            val online: Boolean,
            val completedStep: Boolean,
            val owner: Boolean
    )

    val owner: Participant? get() = participants.find { it.owner }
}

@Serializable
enum class RetrospectiveCommand {
    NEXT_STEP,
    SUBMIT,
    PREVIOUS_STEP,
    STOP_RETRO
}