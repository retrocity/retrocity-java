@file:UseSerializers(WDateTimeSerializer::class, WDateSerializer::class)
@file:OptIn(KlockExperimental::class)

package site.retrocity.common.model

import com.soywiz.klock.annotations.KlockExperimental
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable
data class Team(
    val id: Int,
    var name: String,
    var owner: String,
    var members: Set<String>,
    var metrics: Set<String>,
    var observationGroups: List<ObservationGroup>
)

@Serializable
data class CreateTeamRequest(
    val name: String,
    val members: List<String>,
    val metrics: List<String>,
    val observationGroups: List<ObservationGroup>
)

