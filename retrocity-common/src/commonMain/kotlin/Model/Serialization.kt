package site.retrocity.common.model

import com.soywiz.klock.DateFormat
import com.soywiz.klock.parse
import com.soywiz.klock.parseDate
import com.soywiz.klock.wrapped.WDate
import com.soywiz.klock.wrapped.WDateTime
import com.soywiz.klock.wrapped.wrapped
import kotlinx.serialization.*

@Serializer(forClass = WDate::class)
object WDateSerializer : KSerializer<WDate> {

    override val descriptor: SerialDescriptor
        get() = PrimitiveDescriptor("WithCustomDefault", PrimitiveKind.STRING)

    private val dateFormat = DateFormat("yyyy-MM-dd")

    override fun serialize(encoder: Encoder, value: WDate) {
        encoder.encodeString(value.format(dateFormat))
    }

    override fun deserialize(decoder: Decoder): WDate {
        return dateFormat.parseDate(decoder.decodeString()).wrapped
    }
}

@Serializer(forClass = WDateTime::class)
object WDateTimeSerializer : KSerializer<WDateTime> {

    override val descriptor: SerialDescriptor
        get() = PrimitiveDescriptor("WithCustomDefault", PrimitiveKind.STRING)

    private val dateFormatMillis = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private val dateFormatSeconds = DateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

    override fun serialize(encoder: Encoder, value: WDateTime) {
        encoder.encodeString(value.format(dateFormatMillis))
    }

    override fun deserialize(decoder: Decoder): WDateTime {
        val json = decoder.decodeString()
        return dateFormatMillis.tryParse(json, false)?.utc?.wrapped
            ?: dateFormatSeconds.parse(json).utc.wrapped
    }
}