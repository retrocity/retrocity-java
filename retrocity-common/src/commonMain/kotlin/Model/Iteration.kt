@file:UseSerializers(WDateTimeSerializer::class, WDateSerializer::class)
@file:OptIn(KlockExperimental::class)

package site.retrocity.common.model


import com.soywiz.klock.annotations.KlockExperimental
import com.soywiz.klock.wrapped.WDate
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable
data class Iteration(
    val name: String,
    val startDate: WDate
)



