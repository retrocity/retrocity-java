Retrocity is an application for conducting Sprint Retrospectives.  

At the current time, the application consistents of
* jvm: service with REST & websocket API 
* multiplatform client library
* jvm: cli client application

Requirements:
-
Java 11 or newer
MongoDB

Setup
-
Project is still in incubation and lacks proper configuration settings.
Create a local mongo database retrocity with no credentials

Running
-
./gradlew :retrocity-service:run

