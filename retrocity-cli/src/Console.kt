package site.retrocity.cli

import java.io.InputStream
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.util.*

interface Console {
    fun readLine(): String
    fun readPassword(prompt: String): String
    fun printf(format: String, vararg args: Any)
}

class JavaConsole(private val delegate: java.io.Console) : Console {

    override fun readLine(): String = delegate.readLine()

    override fun readPassword(prompt: String): String {
        printf("%s", prompt)
        return String(delegate.readPassword())
    }

    override fun printf(format: String, vararg args: Any) {
        delegate.printf(format, args)
    }

}

class StreamAdapter(private val input: InputStream, private val output: OutputStream) : Console {

    private val scanner = Scanner(input)

    private val writer = OutputStreamWriter(output)

    override fun readLine(): String =
        scanner.nextLine()

    override fun readPassword(prompt: String): String {
        writer.write("$prompt (warning: password will echo to terminal)")
        writer.flush()
        return scanner.nextLine()
    }

    override fun printf(format: String, vararg args: Any) {
        writer.write(String.format(format, *args))
        writer.flush()
    }

}