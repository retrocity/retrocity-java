package site.retrocity.security

import io.ktor.auth.*
import io.ktor.auth.ldap.*

data class RetrocityPrincipal(val username: String) : Principal


interface ISecurityService {
    fun authenticate(credential: UserPasswordCredential): RetrocityPrincipal?
}

class LdapSecurityService(
        private val ldapServerURL: String,
        private val userDNFormat: String) : ISecurityService {
    override fun authenticate(credential: UserPasswordCredential): RetrocityPrincipal? =
            ldapAuthenticate(credential, ldapServerURL, userDNFormat)
                ?.let { RetrocityPrincipal(credential.name) }
}

class TestSecurityService : ISecurityService {

    private data class TestUser(val username: String, val password: String)

    private val users = listOf(
            TestUser("john", "test"),
            TestUser("jill", "test"))

    override fun authenticate(credential: UserPasswordCredential): RetrocityPrincipal? =
            users.find { it.username == credential.name && it.password == credential.password }
                ?.let { RetrocityPrincipal(it.username) }

}