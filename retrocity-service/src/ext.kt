package site.retrocity.ext

import io.ktor.auth.*
import site.retrocity.security.RetrocityPrincipal

val AuthenticationContext.username
    get() = (principal as RetrocityPrincipal).username